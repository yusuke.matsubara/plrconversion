package plrConversion

import plrConversion.Util.Companion.authenticateAndGetStorage
import plrConversion.Util.Companion.getLogger
import com.assemblogue.plr.contentsdata.PLRContentsData
import com.assemblogue.plr.lib.EntityNode
import com.assemblogue.plr.lib.PLR
import com.assemblogue.plr.lib.model.Channel
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.joda.time.DateTime
import java.io.File
import java.nio.charset.Charset
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.time.temporal.TemporalUnit
import java.util.*

class PushToPlr {
	companion object {
		private val logger by getLogger()

		fun dbTablesFromDirectory(dir: File, charset: Charset, filter: DbTable.Filter): List<DbTable> {
			val list = dir.listFiles()?.filter{it.isFile && !it.isHidden && it.extension == "csv"}
			if (list != null && list.isNotEmpty()) {
				return list.map { file ->
					val d = DbTable.fromHeaderedCsv(file.nameWithoutExtension, file.reader(charset), filter)
					logger.info("Reading $file ... ${d.getColumns().size} x ${d.getRowsAsLists().size}")
					if (d.isEmpty()) {
						null
					} else {
						d
					}
				}.filterNotNull()
			} else {
				error(Util.format("empty_directory", dir))
			}
		}

		fun duplicateExistsNearDateTime(type: String, channel: Channel, amount: Long, unit: TemporalUnit, begin: Instant): Boolean {
			val dup = channel.node.getTimelineItems(
					Date.from(begin),
					2 * amount * unit.duration.toMillis(), true).items.filter{it.type == listOf(type)}.filterNotNull().toSet()
			if (dup.isNotEmpty()) {
				logger.info("$type, $begin: ${dup.map{it.timelineItemTag}}")
			}
			return dup.isNotEmpty()
		}

		//TODO: inefficient
		fun duplicateExists(type: String, channel: Channel) = duplicateExistsNearDateTime(type, channel, 100L, ChronoUnit.YEARS, Instant.ofEpochMilli(0L))

		fun duplicateExistsRecent(type: String, channel: Channel): Boolean {
			val dup = channel.node.getRecentTimelineItems(true).items.filter{ it.type == listOf(type) }.filterNotNull().toSet()
			if (dup.isNotEmpty()) {
				logger.info("$type: ${dup.map{it.timelineItemTag}}")
			}
			return dup.isNotEmpty()
		}

		@JvmStatic
		fun main(args: Array<String>) {
			val props = Util.findConfigOrCreate(
					File("config.properties"),
					mapOf(
							"plr.conversion.friendList" to "./friendlist.csv",
							"plr.conversion.conversionTable" to "./conversionTable.xlsx",
							"plr.conversion.inputDir" to "/tmp/input",
							"plr.conversion.charset" to Charset.defaultCharset().displayName())
			)
			logger.info((props.toMap().minus("plr.conversion.plrPassphrase")).toString())

			val charset: Charset = Charset.forName(props.getProperty("plr.conversion.charset"))
			val friendList = FriendList.load(File(props.getProperty("plr.conversion.friendList")).reader(charset))
			val sheet = WorkbookFactory.create(File(props.getProperty("plr.conversion.conversionTable"))).getSheetAt(0)
			val inputDir = props.getProperty("plr.conversion.inputDir")
			val filter = DbTable.Filter((props.getProperty("plr.conversion.ignoredColumns")
					?: "").split(",").toSet())

			val plr = (PLR.createBuilder() as com.assemblogue.plr.lib.generic.PlrBuilder).build()
			val modified = mutableListOf<EntityNode>()
			try {
				val storage = authenticateAndGetStorage(plr, props.getProperty("plr.conversion.plrId"), props.getProperty("plr.conversion.plrPassphrase"))!!
				val root = plr.getRoot(storage)

				val cd = PLRContentsData(listOf<java.io.InputStream>(), listOf<java.io.InputStream>()) //TODO
				val defs = DefTable.parseTableWithHeading(sheet)
				val creator = NodeCreator(defs.lines, cd)
				logger.fine(creator.toString())
				root.syncAndWait()

				creator.convertDbTablesIntoRecords(dbTablesFromDirectory(File(inputDir), charset, filter)).forEach perUser@ { (user, entries) ->
					val plrId = friendList.findFromDbUserId(user)?.plrId
					if (plrId == null) {
						System.err.println(Util.format("channel_not_found", user))
						return@perUser
					}
					val channel = Util.friendsChannel(root, plrId) { true }
					if (channel == null) {
						System.err.println(Util.format("channel_not_found", plrId))
					} else if (!channel.meta.canWrite) {
						System.err.println(Util.format("channel_not_writable", plrId))
					} else if (entries.isNotEmpty()) {
						creator.prepareNodes(entries).forEach forEachItem@ {
							logger.fine(it.toString())
							val begin = (it.props["begin"] as TypedLiteral?)?.value
							if ((begin != null && begin.isNotEmpty() && duplicateExistsNearDateTime(it.typeName, channel, 1L, ChronoUnit.SECONDS, Instant.ofEpochMilli(DateTime.parse(begin).millis)))
									|| ((begin == null || begin.isEmpty()) && (duplicateExistsRecent(it.typeName, channel) || duplicateExists(it.typeName, channel)))) {
								println(Util.format("item_duplicate", it.toString(), channel))
								return@forEachItem
							} else {
								val item = creator.createNode(channel.node, it)
								item.sync()
								modified.add(item)
								println(Util.format("item_created", item, channel, it.toString()))
							}
						}
						channel.node.syncNewTimelineItems()
						modified.add(channel.node)
					}
				}

			} finally {
				modified.forEach {
					if (it.isDirty || it.inSync() || it.inTimelineSync()) {
						it.syncAndWait(true)
					}
				}
				plr.destroy()
			}
		}
	}
}
