package plrConversion

import com.assemblogue.plr.contentsdata.PLRContentsData
import com.assemblogue.plr.lib.EntityNode
import org.joda.time.Instant
import plrConversion.Util.Companion.getLogger

data class NodeCreator(
		val defs: List<DefLine>,
		val contentsData: PLRContentsData?) {
	private val logger by getLogger()

	private val defsIndexedByTableCol = defs.map{(it.table to it.column) to it}.toMap()
	private val defsIndexedByType = divideDefsByNode(defs).toMap()

	private val userIdColumns = defs.filter{it.flag.isUserKey() }.map{it.table to it.column}.toMap().also {
		if (it.isEmpty()) {
			error("user ID columns not defined: $defs")
		}
	}
	//TODO: ^ warn duplicate table-column pairs
	private val predefinedTypesForProperty = mapOf("begin" to LiteralType.DATETIME)

	fun convertDbTablesIntoRecords(tables: List<DbTable>): List<Pair<String, List<DbRecord>>> {
		if (tables.any{userIdColumns.get(it.name) == null}) {
			logger.warning("some tables lack user ID column: ${tables.filter{userIdColumns.get(it.name) == null}.map{it.name}}; user ID columns: $userIdColumns")
		}
		return tables.flatMap { table ->
			(table.getRowsAsMaps() as List<Map<String, TypedLiteral>>).groupBy {
				val col = userIdColumns[table.name]
				if (col != null) {
					it[col]?.value
				} else {
					null
				}
			}.filterKeys{s ->  s != null}.map { (u, rows) ->
				Triple(u!!, table, rows)
			}
		}.groupBy{(u,_, _) -> u}
				.map{(u, ls) -> u to ls.map{it.second to it.third} }
				.map { (u, ls) ->
					u to ls.flatMap { (table, entries) ->
						entries.map { cols ->
							DbRecord(table.name, cols)
						}
					}
				}
	}

	fun findNode(records: List<DbRecord>): Sequence<Triple<List<TypeDef>, List<String>, List<DbRecord>>> = sequence {
			records.groupBy { it.table }.forEach { (table, records) ->
				val columns = records.flatMap { it.columns.keys.toList() }
				columns.groupBy { k -> defsIndexedByTableCol[table to k]?.nodeHierarchy }.forEach { (types, columns) ->
					if (types != null) {
						if (types.isEmpty()) {
							if (columns.any { it != userIdColumns[table] }) {
								logger.warning("node type is empty: $columns")
							}
						} else {
							if (types.last().typeName == "Event") {
								logger.warning("node type Event in record ($columns)")
							}
							yield(Triple(types, columns, records))
						}
					}
				}
			}
		}

	fun prepareNodes(records: List<DbRecord>): Sequence<TypedEntity> = sequence {

		findNode(records).forEach { (types, columns, records) ->
			val mainType = types.last()
			records.forEach { record ->
				val properties = mutableMapOf<String, TypedValue>()
				val defs = defsIndexedByType[mainType]
				if (!defs!!.all { it.flag.isUserKey() || it.getMainNodeType() == mainType }) {
					error("inconsistent main node types: " + defs.map { it.getMainNodeType() })
				}

				// reversed order so that higher entries take precedence
				columns.reversed().forEach forEachRec@{ col ->
					val def = defsIndexedByTableCol[record.table to col]
					if (def != null) {
						if (def.linkDefs.isEmpty()) {
							if (def.flag == DefFlag.MAPPING) {
								logger.warning("empty link specifiers in $def")
							}
							return@forEachRec
						}
						val linkName = def.linkDefs.first().linkName
						if (linkName.isEmpty()) {
							logger.warning("empty link name: $def")
						}
						val valueType = LiteralType.parseValueType(def.linkDefs.last().endType)
								.ifUnknown(LiteralType.parseValueType(def.valueType))
								.ifUnknown(predefinedTypesForProperty[linkName] ?: LiteralType.UNKNOWN)
						val lit = TypedLiteral(valueType, record.columns[col]!!.value)
						if (lit.value.isNotEmpty()) {
							properties[linkName] = when (def.linkDefs.size) {
								1 -> {
									lit
									//TODO: 型に関してontologyItemを参照する
								}
								else -> {
									if (!properties.contains(linkName)) {
										TypedEntity(def.linkDefs.first().endType!!, emptyMap<String,TypedValue>())
									} else {
										properties[linkName] as TypedEntity
										// TODO: 上書きでなく、Listにして列挙すべき？
									}.replacedEntity(def.linkDefs.drop(1), lit)
								}
							}
						}
					} else {
						logger.warning("not found: ${record.table}, $col")
					}
				}
				logger.fine("properties@$mainType: $properties")
				if (properties.isNotEmpty()) {
					yield(TypedEntity(mainType.typeName, properties))
				}
			}
		}
	}

	fun createNode(parent: EntityNode, entity: TypedEntity): EntityNode {
		val node = parent.newTimelineItem(entity.typeName, Instant.now())
		val creator = if (defs.any { it.flag == DefFlag.USER_ID_SUBJECT_WRITABLE }) {
			"${parent.storage.type.name}:${parent.meta.owner}"
		} else {
			"${parent.storage.type.name}:${parent.storage.userId}"
		}
		// TODO: 書き込み許可を与える機能がまだないので、creatorを切り替えている
		node.newLiteral("creator").value = creator
		entity.props.forEach { (key, value) ->
			createLiteralOrInnerEntity(node, key, value)
		}
		return node
	}

	@Deprecated("to be removed")
	fun createNodes(parent: EntityNode, records: List<DbRecord>): Sequence<EntityNode> = sequence {
		prepareNodes(records).forEach { entity ->
			val node = createNode(parent, entity)
			yield(node)
		}
	}

	fun createLiteralOrInnerEntity(parent: EntityNode, key: String, value: TypedValue) {
		when (value) {
			is TypedLiteral -> {

				if (value.type == LiteralType.UNKNOWN) {
					logger.warning("unknown type used for '$key' in $parent")
				}
				if (key == "begin") {
					parent.setBegin(value.objectify())
				} else {
					parent.newLiteral(key).also { newNode ->
						newNode!!.value = value.objectify()
					}
				}
			}
			is TypedEntity -> parent.newInnerEntity(key, value.typeName).also { newNode ->
				value.props.forEach { (k, v) ->
					createLiteralOrInnerEntity(newNode!!, k, v)
				}
			}
		}
	}

	companion object {

		fun validatePropertyName(s: String): Boolean {
			return s[0].isLetter() && s[0].isLowerCase()
		}

		fun divideDefsByNode(defs: List<DefLine>): List<Pair<TypeDef, List<DefLine>>> {
			return defs.mapIndexed{i, x -> i to x }.groupBy{it.second.getMainNodeType()}.toList()
					.filter { it.first != null }
					.map { (type, ls) -> type!! to ls.sortedBy { it.first }.map{it.second} }
		}
	}
}