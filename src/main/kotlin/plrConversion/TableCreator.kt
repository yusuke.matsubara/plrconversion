package plrConversion

import com.assemblogue.plr.contentsdata.PLRContentsData
import com.assemblogue.plr.lib.EntityNode
import com.assemblogue.plr.lib.Node
import org.joda.time.DateTime
import plrConversion.Util.Companion.getLogger

data class TableCreator(
		val defs: DefTable,
		val contentsData: PLRContentsData?) {
	private val logger by getLogger()

	private val updaters: List<(EntityNode) -> List<DbRecord>> = defs.lines.filter{it.flag == DefFlag.MAPPING }.groupBy { it.getMainNodeType() }.filter{ (type, _) -> type != null}.map { (_, defs) ->
		buildMappingFunction(defs)
	}
	private val userIdColumns: Map<String,String> = defs.lines.filter{it.flag.isUserKey() }.map{it.table to it.column}.toMap()

	private fun buildMappingFunction(defs: List<DefLine>): (EntityNode) -> List<DbRecord> {

		return { item ->
			val cols = mutableListOf<Pair<Pair<String,String>, TypedLiteral>>()
			defs.forEach forEachDef@ { def ->
				if (item.type != listOf(def.getMainNodeType()?.typeName)) {
					return@forEachDef
				}
				var currentNodes: List<Node> = listOf(item)

				if (currentNodes.size == 1 && currentNodes[0].isEntity() && def.linkDefs.size == 1 && def.linkDefs.first().link == "begin" && currentNodes[0].asEntity().begin.isNotEmpty()) {
					cols.add((def.table to def.column) to TypedLiteral.of(DateTime(currentNodes[0].asEntity().begin.first())))
					// TODO: すでに値があった場合は警告を表示
					return@forEachDef
				}

				def.linkDefs.forEach { linkDef ->
					if (currentNodes.any{ it.isEntity() && it.asEntity().hasProperty(linkDef.linkName) }) {
						val a = currentNodes.flatMap{ it.asEntity().getProperty(linkDef.linkName) }
						currentNodes = a
						//TODO: 型チェック
					} else {
						logger.warning("link not found, skipping - $def.linkDefs; $item")
						return@forEachDef
					}
				}

				val endType = def.findEndType()
				if (currentNodes.size >= 2) {
					logger.warning("multiple candidate nodes for $def: $currentNodes")
				}
				// we should have reached to a literal node, whose value should go to the right table/column.
				if (endType == LiteralType.MULTIMEDIA) {
					if (currentNodes.none{it.isFile()}) {
						logger.warning("no file node for $def: $currentNodes")
					} else {
						val count = currentNodes.count { it.isFile() }
						cols.add((def.table to def.column) to TypedLiteral(LiteralType.INT, count.toString()))
					}
				} else if (endType != null) {
					if (currentNodes.none{it.isLiteral()}) {
						logger.warning("no literal node for $def: $currentNodes")
					} else {
						val node = currentNodes.find { it.isLiteral() }!!.asLiteral()
						cols.add((def.table to def.column) to TypedLiteral(LiteralType.of(node), node.value.toString()))
					}
				} else {
					logger.warning("not literal nor file: $currentNodes")
				}
			}
			cols.groupBy{it.first.first}.map{ (table, ls) ->
				DbRecord(table, ls.map { it.first.second to it.second }.toMap())
			}
		}
	}

	fun createTables(items: Map<String, List<EntityNode>>): Map<String, DbTable> {
		return items.flatMap { (user, items) ->
			val entries = updaters.flatMap { update ->
				items.flatMap { item ->
					logger.info(item.toString())
					update(item)
				}
			}
			logger.info("$user, $entries")
			entries.groupBy{ it.table }.map{ (tableName, records) ->
				val table = DbTable(tableName)
				records.forEach { record ->
					table.addRow(record.columns + if (userIdColumns.containsKey(tableName)) {
						mapOf(userIdColumns.getValue(tableName) to TypedLiteral.of(user))
					} else {
						mapOf()
					})
				}
				table
			}
		}.groupBy { it.name  }.map { (name, tables) ->
			val table = tables[0]
			tables.slice(1 until tables.size).forEach { it.getRowsAsMaps().forEach { table.addRow(it) }}
			name to table
		}.toMap()
	}

	override fun toString(): String {
		return """plrConversion.TableCreator(
			${defs.lines.joinToString { "${it.table}.${it.column} -> ${it.linkDefs},\n" }}
		)""".trimMargin()
	}

	companion object {

		fun validatePropertyName(s: String): Boolean {
			return s[0].isLetter() && s[0].isLowerCase()
		}
	}
}
