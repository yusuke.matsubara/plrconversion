package plrConversion

import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import org.apache.commons.csv.CSVPrinter
import java.io.Reader
import java.io.Writer
import java.util.*
import plrConversion.Util.Companion.getLogger

class DbTable(val name: String) {
	private val columns: SortedSet<String> = TreeSet<String>()
	private val rows = mutableListOf<Map<String, TypedLiteral>>()

	override fun equals(other: Any?): Boolean {
		when (other) {
			is DbTable ->
				return name == other.name && columns == other.columns && rows == other.rows
		}
		return false
	}

	override fun hashCode(): Int {
		return name.hashCode() + columns.hashCode() + rows.hashCode()
	}

	override fun toString(): String {
		return "${super.toString()}:$name($columns, ${getRowsAsLists()})"
	}
	fun addRow(row: Map<String, TypedLiteral>): DbTable {
		row.forEach { (col, _) ->
			if (!columns.contains(col)) {
				columns.add(col)
			}
		}
		rows.add(row)
		return this
	}

	fun getColumns() = columns.map{it}

	fun getRowsAsLists(): List<List<TypedLiteral?>> {
		return rows.map { row -> getColumns().map { row[it] } }
	}

	fun getRowsAsMaps(): List<SortedMap<String, TypedLiteral>> {
		return rows.map{ it.toSortedMap() }
	}

	fun shrink(): DbTable {
		val table = DbTable(name)
		val buff = mutableMapOf<String, TypedLiteral>()
		sequence {
			rows.forEach { row ->
				if (row.all { (col, value) ->
							!buff.containsKey(col) || buff[col] == value }) {
					buff.putAll(row)
				} else {
					yield(buff.toMap())
					buff.clear()
					buff.putAll(row)
				}
			}
			if (buff.isNotEmpty()) {
				yield(buff.toMap())
			}
		}.forEach { table.addRow(it) }
		return table
	}

	fun isEmpty() = rows.isEmpty()

	fun toHeaderedCsv(writer: Writer, filter: Filter = Filter(setOf())) {
		val printer = CSVPrinter(writer, CSVFormat.DEFAULT.withRecordSeparator(System.lineSeparator()))
		printer.printRecord(columns - filter.ignoredColumns)
		val ignored = columns.mapIndexed{i, c -> i to (c in filter.ignoredColumns)}.toMap()
		getRowsAsLists().forEach { row ->
			printer.printRecord(row.filterIndexed{i, _ -> !(ignored[i] ?: false)}.map{it?.value})
		}
		printer.close()
	}

	companion object {
		private val logger by getLogger()

		fun fromHeaderedCsv(name: String, reader: Reader, filter: Filter = Filter(setOf()), defs: DefTable? = null): DbTable {
			val parser = CSVParser.parse(reader, CSVFormat.DEFAULT.withHeader())
			val table = DbTable(name)
			val columns = parser.headerNames.toList()
			val columnWarned = mutableSetOf<Pair<String, String>>()
			val parseValue = if (defs == null) {
				_, s -> TypedLiteral.guessAndParse(s)
			} else {
				fun (column: String, s: String): TypedLiteral {
					val type = defs.findEndTypeForTableColumn(name, column)
					return if (type == null) {
						if ((name to column) !in columnWarned) {
							logger.warning("column not defined: $name, $column")
							columnWarned.add(name to column)
						}
						TypedLiteral.guessAndParse(s)
					} else {
						TypedLiteral(type, s)
					}
				}
			}

			table.columns.addAll(columns)
			parser.forEach { record ->
				table.addRow(record.mapIndexed{i, v -> columns[i] to parseValue(columns[i], v)}
						.filterNot{(k,_) -> k in filter.ignoredColumns}.toMap())
			}
			parser.close()

			table.columns.removeIf{it in filter.ignoredColumns}
			return table
		}
		fun fromHeaderedCsv(name: String, reader: Reader, defs: DefTable) = fromHeaderedCsv(name, reader, Filter(emptySet()), defs)
	}

	data class Filter(val ignoredColumns: Set<String>)
}

