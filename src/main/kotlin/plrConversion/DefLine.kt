package plrConversion

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row

enum class DefFlag {
	USER_ID,
	USER_ID_SUBJECT_WRITABLE,
	MAPPING;

	fun isUserKey(): Boolean = this == USER_ID || this == USER_ID_SUBJECT_WRITABLE
}

data class DefLine(
		val flag: DefFlag,
		val table: String,
		val column: String,
		val nodeHierarchy: List<TypeDef>,
		val valueType: String?,
		val comment: String?,
		val linkDefs: List<LinkDef>) {
	fun getMainNodeType(): TypeDef? = if (nodeHierarchy.isNotEmpty()) { nodeHierarchy.last() } else { null }

	fun findEndType(): LiteralType? {
		return when {
			flag.isUserKey() -> {
				LiteralType.STR
			}
			linkDefs.isEmpty() -> {
				null
			}
			else -> {
				LiteralType.parseValueType(linkDefs.last().endType)
			}
		}
	}

	companion object {

		fun validateTypeName(s: String): Boolean =
				s.isNotEmpty() && s[0].isLetter() && s[0].isUpperCase()

		fun validatePropertyName(s: String): Boolean =
				s.isNotEmpty() && s[0].isLetter() && s[0].isLowerCase()

		fun splitNodeHierarchyAndLink(list: List<String>): Pair<List<String>, List<String>> {
			val n = list.indexOfFirst { validatePropertyName(it) }
			return if (n >= 1) {
				Pair(
						list.slice(0 until n),
						list.slice(n until list.size).filter{ validatePropertyName(it) || validateTypeName(it) }
				)
			} else {
				Pair(listOf(), listOf())
			}
		}
	}
}

data class DefTable(val lines: List<DefLine>) {
	fun findEndTypeForTableColumn(table: String, column: String): LiteralType? {
		val def = lines.find{it.table == table && it.column == column}
		return def?.findEndType()
	}
	companion object {

		fun fillEmptyCellsWithAbove(rows: List<List<String>>, from: Int, to: Int?): List<List<String>> {
			val previous = arrayListOf<String>()
			return rows.map{ row ->
				row.mapIndexed{ cind, v ->
					if (cind < from || cind >= (to ?: row.size)) {
						v
					} else if (v.isEmpty() && cind < row.size - 1) {
						previous.getOrElse(cind) { v }
					} else if (v.isEmpty()) {
						(cind + 1 until previous.size).reversed().forEach{ i -> previous.removeAt(i)}
						v
					} else {
						(cind + 1 until previous.size).reversed().forEach{ i -> previous.removeAt(i)}
						if (cind >= previous.size) {
							previous.addAll(generateSequence{""}.take(cind + 1 - previous.size))
						}
						previous[cind] = v

						v
					}
				}
			}
		}
		fun fillWithFullTypeDefinitions(rows: List<List<String>>, from: Int, to: Int?): List<List<String>> {
			val restrictionPos = rows.indexOfFirst { it.isNotEmpty() && it[0] == "R" }
			val types = rows.slice(0 until (if (restrictionPos >= 0) restrictionPos else rows.size))
					.flatMap { row -> row.subList(from.coerceAtMost(to ?: row.size), to ?: row.size).map{
						val t = TypeDef(it)
						t.typeName to t
					} }.groupBy { it.first }.map { (k, v) ->
						val ls = v.map{it.second}
						k to ls.maxBy { it.type.length }!!
					}.toMap()
			return rows.map{ row -> row.subList(0, from.coerceAtMost(to ?: row.size)) + row.subList(from.coerceAtMost(to ?: row.size), to ?: row.size).map{v ->
				if (types[v] != null) {
					types[v]!!.type
				} else {
					v
				}
			} + row.subList(to ?: row.size, row.size)}
		}
		fun rowsToIndexedList(rows: List<Row>): List<List<String>> {
			return rows.mapIndexed { rind, row ->
				rowToIndexedList(row).mapIndexed{ cind, cell ->
					try {
						if (cell != null) {
							TypedLiteral.of(cell).value
						} else {
							""
						}
					} catch (e: NotImplementedError) {
						System.err.println("Cell ($rind, $cind) cannot be parsed: $cell")
						""
					}
				}
			}
		}
		private fun rowToIndexedList(row: Row): List<Cell?> {
			val ret = Array<Cell?>(Integer.max(row.lastCellNum.toInt(), 0)){ null }
			row.forEach{
				cell -> ret[cell.columnIndex] = cell
			}
			return ret.toList()
		}
		fun parseTableWithHeading(sheet: Iterable<Row>): DefTable {
			val origRows = sheet.toList()
			val rows = rowsToIndexedList(origRows).filter{ it.first().isNotEmpty() }
			val heading = rows.first().map { TypedLiteral.of(it) }
			val tablePos = heading.indexOfFirst { it.value.matches(Regex("^[tT][a-zA-Z]*$")) }
			val columnPos = heading.indexOfFirst { it.value.matches(Regex("^[cC][a-zA-Z]*$")) }
			val specPos = heading.indexOfFirst { it.value.startsWith("]") }
			val rowsBody = rows.drop(1)
			if (tablePos < 0 || columnPos < 0 || specPos < 0 || rows.isEmpty()) {
				error("table $tablePos, column $columnPos, spec $specPos, rows $rows")
			}
			return parseTable(fillWithFullTypeDefinitions(
					fillEmptyCellsWithAbove(
							fillEmptyCellsWithAbove(rowsBody, specPos, null),
							tablePos, tablePos + 1),
					specPos, null).filter { it.isNotEmpty() }, tablePos, columnPos, specPos)
		}
		fun parseTable(rows: List<List<String>>, tablePos: Int, columnPos: Int, specPos: Int): DefTable {
			return DefTable(sequence {
				rows.forEach { row ->
					if (row.isEmpty()) {
						return@forEach
					}
					val flag = when (row[0]) {
						"" -> return@forEach
						"K" -> DefFlag.USER_ID
						"B" -> DefFlag.MAPPING
						"H" -> return@forEach
						"S" -> DefFlag.USER_ID_SUBJECT_WRITABLE
						"R" -> return@forEach
						else -> TODO("tag $row[0]")
					}
					val (h, l) = DefLine.splitNodeHierarchyAndLink(row.slice(specPos until row.size))
					val specs = l.chunked(2).map { LinkDef(it[0], it.getOrNull(1)) }
					yield(DefLine(flag, row[tablePos], row[columnPos], h.map { TypeDef(it) }, null, null, specs))
				}
			}.toList())
		}
	}
}
