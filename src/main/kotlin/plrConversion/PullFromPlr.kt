package plrConversion

import plrConversion.Util.Companion.authenticateAndGetStorage
import plrConversion.Util.Companion.friendsChannels
import plrConversion.Util.Companion.getLogger
import com.assemblogue.plr.lib.PLR
import com.assemblogue.plr.lib.model.Channel
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.apache.poi.ss.usermodel.WorkbookFactory
import java.io.File
import java.nio.charset.Charset
import java.nio.file.Paths
import java.time.Instant
import java.util.*

/**
 * テスト
 */
class PullFromPlr {
	companion object {
		private val logger by getLogger()

		fun generateTablesFromChannels(defs: DefTable, channels: List<Channel>, friendList: FriendList, begin: Instant): Map<String, DbTable> {
			val creator = TableCreator(defs, null)
			val items = channels.filter{friendList.findFromPlrId(it.plrId) != null}.groupBy {
				logger.info("$it -> ${friendList.findFromPlrId(it.plrId)}")
				friendList.findFromPlrId(it.plrId)!!.dbUserId
			}.map{ (user, channels) ->
				user to channels.flatMap { channel ->
					channel.syncAndWait()
					channel.node.getTimelineItems(Date.from(begin), Instant.now().toEpochMilli() - begin.toEpochMilli(), true).items
				}
			}
			return creator.createTables(items.toMap())
		}

		@JvmStatic
		fun main(args: Array<String>) = runBlocking {
			val props = Util.findConfigOrCreate(
					File("config.properties"),
					mapOf(
							"plr.conversion.friendList" to "./friendlist.csv",
							"plr.conversion.conversionTable" to "./conversionTable.xlsx",
							"plr.conversion.pullBegin" to "P-3D",
							"plr.conversion.outputDir" to "/tmp/",
							"plr.conversion.charset" to Charset.defaultCharset().displayName())
			)
			logger.info((props.toMap().minus("plr.conversion.plrPassphrase")).toString())

			val charset: Charset = Charset.forName(props.getProperty("plr.conversion.charset"))
			val friendList = FriendList.load(File(props.getProperty("plr.conversion.friendList")).reader(charset))
			val sheet = WorkbookFactory.create(File(props.getProperty("plr.conversion.conversionTable"))).getSheetAt(0)
			val outputDir = props.getProperty("plr.conversion.outputDir")
			val filter = DbTable.Filter((props.getProperty("plr.conversion.ignoredColumns")
					?: "").split(",").toSet())

			val plr = (PLR.createBuilder() as com.assemblogue.plr.lib.generic.PlrBuilder).build()
			try {
				val storage = authenticateAndGetStorage(plr, props.getProperty("plr.conversion.plrId"), props.getProperty("plr.conversion.plrPassphrase"))!!
				val root = plr.getRoot(storage)
				root.syncAndWait()
				val channels = friendsChannels(root) { true }.toList(mutableListOf())
				val defs = DefTable.parseTableWithHeading(sheet)
				logger.fine("$defs")
				generateTablesFromChannels(defs, channels, friendList, Util.parseDateTimeAbsoluteOrRelative(props.getProperty("plr.conversion.pullBegin"))).forEach { (name, table) ->
					val file = Paths.get(outputDir, "$name.csv")
					logger.info("output: $file")
					table.shrink().toHeaderedCsv(file.toFile().writer(charset), filter)
				}
			} finally {
				plr.destroy()
			}
		}
	}
}
