package plrConversion

import com.assemblogue.plr.io.Storage
import com.assemblogue.plr.lib.model.Root
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.runBlocking
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import org.apache.commons.csv.CSVPrinter
import java.io.Reader
import java.io.Writer
import java.time.Instant

class FriendList {
	data class Entry(val plrId: String, val dbUserId: String, val lastModified: Instant)

	private val plrToDb = mutableMapOf<String, Entry>()
	private val dbToPlr = mutableMapOf<String, Entry>()

	fun findFromPlrId(plrId: String): Entry? =
			plrToDb[plrId]
	fun findFromPlrId(plrId: String, type: Storage.Type<Any>): Entry? =
			plrToDb[type.name + ":" + plrId]
	fun findFromDbUserId(dbUserId: String): Entry? =
			dbToPlr[dbUserId]
	fun add(plrId: String, dbUserId: String, time: Instant = Instant.now()) {
		//TODO validate plr id
		val e = Entry(plrId, dbUserId, time)
		plrToDb[plrId] = e
		dbToPlr[dbUserId] = e
	}
	fun saveToFile(writer: Writer) {
		val printer = CSVPrinter(writer, CSVFormat.DEFAULT)
		plrToDb.forEach { (_, entry) ->
			printer.printRecord(entry.plrId, entry.dbUserId, entry.lastModified.toString())
		}
	}
	fun allPlrIds() = plrToDb.keys.toSet()

	companion object {
		fun load(reader: Reader): FriendList {
			val list = FriendList()
			val parser = CSVParser.parse(reader, CSVFormat.DEFAULT)
			parser.forEach { record ->
				when (val size = record.size()) {
					3 -> {
						val (x, y, z) = record.toList()
						list.add(x, y, Instant.parse(z))
					}
					2 -> {
						val (x, y) = record.toList()
						list.add(x, y, Instant.now())
					}
					else -> {
						System.err.println("$size records present; should be 2 or 3")
					}
				}
			}
			return list
		}

		fun findFromRoot(root: Root, type: String, property: String): FriendList = runBlocking {
			val list = FriendList()
			Util.friendsChannels(root){true}.collect { channel ->
				channel.node.getRecentTimelineItems(listOf(type)).items.forEach { item ->
					val v = item.getProperty(property)
					if (v.size == 1) {
						list.add(channel.ownerPlrId, v[0].asLiteral().value.toString())
					}
				}
			}
			list
		}
	}
}