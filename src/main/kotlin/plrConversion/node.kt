package plrConversion

import com.assemblogue.plr.lib.FileNode
import com.assemblogue.plr.lib.LiteralNode
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.CellType
import org.joda.time.DateTime
import org.joda.time.Duration

enum class LiteralType {
	INT, STR, FLOAT, DATETIME, DURATION, MULTIMEDIA, UNKNOWN;

	fun unionType(type: LiteralType?): LiteralType =
		if (type == null) {
			this
		} else when (val v = this to type) {
			INT to FLOAT, FLOAT to INT -> FLOAT
			else -> if (v.first == v.second) {
				v.first
			} else if (v.first == STR || v.second == STR) {
				STR
			} else {
				UNKNOWN
			}
		}

	fun intersectionType(type: LiteralType?): LiteralType =
		if (type == null) {
			this
		} else when (val v = this to type) {
			INT to FLOAT, FLOAT to INT -> INT
			else -> when {
				v.first == v.second -> {
					v.first
				}
				v.first == STR -> {
					v.second
				}
				v.second == STR -> {
					v.first
				}
				else -> {
					UNKNOWN
				}
			}
		}

	fun ifUnknown(type: LiteralType): LiteralType =
		if (this == UNKNOWN) {
			type
		} else {
			this
		}

	companion object {
		fun parseValueType(s: String?): LiteralType {
			return when (s) {
				"integer", "nonNegativeInteger", "Integer", "INT"-> INT
				"string", "String", "STR" -> STR
				"float", "Float", "FLOAT" -> FLOAT
				"mmdata", "Mmdata" -> MULTIMEDIA
				"datetime", "dateTime", "DateTime", "DATETIME" -> DATETIME
				"duration", "Duration", "DURATION" -> DURATION
				else -> UNKNOWN
			}
		}

		fun of(node: FileNode): LiteralType = MULTIMEDIA

		fun of(node: LiteralNode): LiteralType {
			return when (node.type) {
				listOf("int"), listOf("Integer") -> INT
				listOf("string"), listOf("String") -> STR
				listOf("float"), listOf("Float") -> FLOAT
				listOf("datetime"), listOf("DateTime") -> DURATION
				listOf("duration"), listOf("Duration") -> DURATION
				else -> UNKNOWN
			}
		}
	}
}

abstract class TypedValue
data class TypedLiteral(val type: LiteralType, val value: String): TypedValue() {
	fun objectify(): Any {
		return when (type) {
			LiteralType.INT ->   value.trim().toInt()
			LiteralType.FLOAT -> value.trim().toFloat()
			LiteralType.DATETIME -> DateTime.parse(value.trim()).toString()
			LiteralType.DURATION -> Duration.parse(value.trim()).toString()
			LiteralType.STR -> value
			LiteralType.MULTIMEDIA -> value
			LiteralType.UNKNOWN -> value
		}
	}

	companion object {
		fun guessAndParse(s: String): TypedLiteral {
			try {
				return of(s.toInt())
			} catch (e: NumberFormatException) {
			}
			try {
				return of(s.toFloat())
			} catch (e: NumberFormatException) {
			}
			try {
				return of(Duration.parse(s))
			} catch (e: IllegalArgumentException) {
			}
			if (s.matches(Regex("^\\d{4}-\\d{2}-\\d{2}($|[T ].*)"))) {
				try {
					return of(DateTime.parse(s))
				} catch (e: IllegalArgumentException) {
				}
			}
			return of(s)
		}
		fun of(n: Int) = TypedLiteral(LiteralType.INT, n.toString())
		fun of(x: Float) = TypedLiteral(LiteralType.FLOAT, x.toString())
		fun of(x: DateTime) = TypedLiteral(LiteralType.DATETIME, x.toString())
		fun of(x: Duration) = TypedLiteral(LiteralType.DURATION, x.toString())
		fun of(s: String) = TypedLiteral(LiteralType.STR, s)
		fun of(cell: Cell?): TypedLiteral {
			return if (cell == null) {
				TypedLiteral(LiteralType.UNKNOWN, "")
			} else when (cell.cellType) {
				CellType.STRING -> TypedLiteral(LiteralType.STR, cell.stringCellValue)
				CellType.NUMERIC -> TypedLiteral(LiteralType.FLOAT, cell.numericCellValue.toString())
				CellType._NONE -> TypedLiteral(LiteralType.UNKNOWN, cell.stringCellValue)
				CellType.FORMULA -> TypedLiteral(LiteralType.STR, cell.stringCellValue)
				CellType.BLANK -> TypedLiteral(LiteralType.STR, "")
				CellType.BOOLEAN -> TODO()
				CellType.ERROR -> TODO()
				null -> TypedLiteral(LiteralType.UNKNOWN, "")
			}
		}
		fun listOf(list: List<Any>): List<TypedLiteral> = list.map {
			when (it) {
				is Int -> of(it)
				is Float -> of(it)
				is DateTime -> of(it)
				is Duration -> of(it)
				is String -> of(it)
				else -> TypedLiteral(LiteralType.UNKNOWN, it.toString())
			}
		}
	}
}

data class TypedEntity(val typeName: String, val props: Map<String, TypedValue>): TypedValue() {
	fun replacedEntity(specs: List<LinkDef>, value: TypedLiteral): TypedEntity =
		when (specs.size) {
			0 -> error("empty specifier")
			1 -> {
				//TODO: 型が一致しているかチェック
				copy(props=props.plus(specs.first().linkName to value))
			}
			else -> {
				val link = specs.first().linkName
				val e = (
					if (props.contains(link)) {
						props[link] as TypedEntity
					} else {
						TypedEntity(specs.first().endType!!, emptyMap())
				}).replacedEntity(specs.drop(1), value)
				copy(props = props.minus(link).plus(link to e))
			}
		}
}
