package plrConversion

import plrConversion.Util.Companion.authenticateAndGetStorage
import plrConversion.Util.Companion.loadJsonMultiMap
import plrConversion.Util.Companion.friendsChannels
import plrConversion.Util.Companion.getLogger
import com.assemblogue.plr.lib.EntityNode
import com.assemblogue.plr.lib.PLR
import kotlinx.coroutines.flow.collectIndexed
import kotlinx.coroutines.runBlocking
import org.apache.poi.ss.usermodel.WorkbookFactory
import java.io.File
import java.lang.Exception
import java.lang.IllegalArgumentException
import java.nio.charset.Charset
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*

class Debug {
	companion object {
		private val logger by getLogger()

		fun chooseFrom(message: String, list: List<String>): String {
			print("$message ${list}: ")
			val ret = Scanner(System.`in`).nextLine()
			val cand = list.filter{ret != null && it.startsWith(ret)}
			return if (ret != null && ret.isEmpty() && cand.size == 1) {
				cand[0]
			} else {
				ret
			}
		}

		@JvmStatic
		fun main(args: Array<String>) = runBlocking {
			val props = Util.findConfigOrCreate(
					File("config.properties"),
					mapOf(
							"plr.conversion.friendList" to "./friendlist.csv",
							"plr.conversion.conversionTable" to "./conversionTable.xlsx",
							"plr.conversion.massDelete" to null,
							"plr.conversion.charset" to Charset.defaultCharset().displayName())
			)
			val charset: Charset = Charset.forName(props.getProperty("plr.conversion.charset"))
			val friendList = FriendList.load(File(props.getProperty("plr.conversion.friendList")).reader(charset))
			val sheet = WorkbookFactory.create(File(props.getProperty("plr.conversion.conversionTable"))).getSheetAt(0)

			val plr = (PLR.createBuilder() as com.assemblogue.plr.lib.generic.PlrBuilder).build()
			val modified = mutableListOf<EntityNode>()
			try {
				val storage = authenticateAndGetStorage(plr, props.getProperty("plr.conversion.plrId"), props.getProperty("plr.conversion.plrPassphrase"))!!
				val root = plr.getRoot(storage)
				root.sync()

				when (val subcommand = chooseFrom("choose sub command", listOf("delete", "list", "mass_delete"))) {
					"list", "delete" -> {
						val type = chooseFrom("choose type", listOf())
						root.syncAndWait()

						friendsChannels(root) { friendList.findFromPlrId(it.plrId) != null }.collectIndexed { channel_index, channel ->
							println("${channel.owner} $channel")
							val items = channel.node.getTimelineItems(
									Date.from(Instant.ofEpochMilli(0L)),
									100 * ChronoUnit.YEARS.duration.toMillis(), true).items.filter { it.type == listOf(type) }
							items.forEachIndexed { index, item ->
								item.syncAndWait()
								val version = if (item.hasVersion()) { "v${item.versionNames().size}" } else { "" }
								println("$channel_index ${index+1}/${items.size} ${item.timelineItemTag} ($version): ${item.allProperties}")
								if (subcommand == "delete") {
									when (chooseFrom("delete? ", listOf("y", "n"))) {
										"y" -> {
											item.isDeleted = true
											item.sync()
											modified.add(item)
										}
									}
								}
							}
							channel.node.sync()
							if (channel.node.isDirty || channel.node.inSync()) {
								modified.add(channel.node)
							}
						}
					}
					"mass_delete" -> {
						val targets = loadJsonMultiMap(File(props.getProperty("plr.conversion.massDelete")).reader())
						val processed = mutableListOf<String>()
						friendsChannels(root) { friendList.findFromPlrId(it.plrId) != null }.collectIndexed { channel_index, channel ->
							val plrId = "${channel.storage.type.name}:${channel.meta.owner}"
							println("$plrId $channel")
							(targets[plrId] ?: emptyList()).forEach { target ->
								try {
									channel.node.getTimelineItemByTag(target, object: PLR.Callback<EntityNode>{
										override fun onResult(item: EntityNode) {
											when {
												item.isDeleted -> {
													println("'$target' is already deleted")
													processed.add(target)
												}
												else -> {
													if (item.versionNames().size > 0) {
														println("'$target' has multiple versions")
														return
													}
													item.isDeleted = true
													item.sync()
													modified.add(item)
													println("'$target' has been deleted")
													processed.add(target)
												}
											}
										}

										override fun onError(e: Exception) {
											println("error: $e")
										}
									})
								} catch (e: IllegalArgumentException) {
									println("'$target' is invalid or not found: $e")
								}
							}
							channel.node.sync()
							if (channel.node.isDirty || channel.node.inSync()) {
								modified.add(channel.node)
							}
						}
						println("remainder: ${targets.flatMap{(k,v) -> v} - processed}")
					}
					else ->	error("unknown subcommand: '$subcommand'")
				}
			} finally {
				modified.forEach {
					if (it.isDirty || it.inSync() || it.inTimelineSync()) {
						it.syncAndWait(true)
					}
				}
				plr.destroy()
			}
		}
	}
}
