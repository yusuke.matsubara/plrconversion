package plrConversion

import XMLResourceBundleControl
import com.assemblogue.plr.io.Storage
import com.assemblogue.plr.lib.PLR
import com.assemblogue.plr.lib.model.Channel
import com.assemblogue.plr.lib.model.Friend
import com.assemblogue.plr.lib.model.Root
import com.fasterxml.jackson.databind.ObjectMapper
import com.healthmarketscience.jackcess.Database
import com.healthmarketscience.jackcess.DatabaseBuilder
import com.healthmarketscience.jackcess.util.ExportUtil
import kotlinx.coroutines.flow.*
import org.joda.time.DateTime
import java.text.MessageFormat
import java.util.*
import java.io.File
import java.io.Reader
import java.time.Duration
import java.time.Instant
import java.time.format.DateTimeParseException
import java.util.logging.Logger

class Util {
	companion object {
		private val res: Res by lazy {
			Res()
		}

		fun format(messageId: String, vararg args: Any) = res.format(messageId, *args)

		fun exportAllFromDbToCsv(from: File, to: File) {

			val db: Database = DatabaseBuilder.open(from)
			db.tableNames.forEach { name ->
				val table = db.getTable(name)
				table.columns.forEach { column ->
					println("${column.name} ${column.length}")
				}
			}
			ExportUtil.exportAll(db, to, "csv", true)
		}

		fun <R : Any> R.getLogger(): Lazy<Logger> {
			return lazy { Logger.getLogger(this.javaClass.name) }
		}


		fun parseDateTimeAbsoluteOrRelative(d: String, baseInstant: Instant = Instant.now()): Instant {
			return try {
				baseInstant.plusMillis(Duration.parse(d).toMillis())
			} catch (e: DateTimeParseException) {
				Instant.ofEpochMilli(DateTime.parse(d).millis)
			}
		}

		fun parseDateTimeAbsolute(d: String): Instant {
			return Instant.ofEpochMilli(DateTime.parse(d).millis)
		}

		fun getPassphraseEntered(): String {
			println(res.format("enter_passphrase"))
			return readLine()!!
		}

		fun friendsChannel(root: Root, plrId: String, predicate: (Channel) -> Boolean): Channel? {
			val friend = root.getFriend(plrId)
			if (friend == null) {
				println("$plrId not found" )
				return null
			}
			println(res.format("sync_friend", friend))
			friend.friendToMeRoot.syncAndWait()
			return if (friend.friendToMeRoot.hasReferencingChannels()) {
				val channels = friend.friendToMeRoot.listReferencingChannels().filter(predicate)
				when {
					channels.size > 1 || channels.isEmpty() -> null
					else -> channels.first()
				}
			} else {
				null
			}
		}

		/**
		 * フレンドが自分に開示しているチャネル全てを収集し、返す
		 *
		 * @param root 自分のルート
		 * @param predicate フレンド(引数)を収集の対象に含めるかどうかの判定関数
		 */
		fun friendsChannels(root: Root, predicate: (Friend) -> Boolean): Flow<Channel> = root.listFriends().asFlow()
				.filter{predicate(it)}.flatMapConcat { friend ->
					println(res.format("sync_friend", friend))
					friend.friendToMeRoot.syncAndWait()
					if (friend.friendToMeRoot.hasReferencingChannels()) {
						friend.friendToMeRoot.listReferencingChannels()
					} else {
						emptyList()
					}.asFlow()
				}

		fun authenticateAndGetStorage(plr: PLR, plrId: String, passphrase: String?): Storage? {
			val (storageType, userId) = try {
				plrId.split(":", limit = 2).let{ a -> Pair(a[0], a[1])}
			} catch (e: IndexOutOfBoundsException) {
				error("invalid user id $plrId; use this format: googleDrive:foobar@gmail.com")
			}
			val storages = plr.listStorages().filter {
				x -> x.typeName == storageType && x.userId == userId
			}

			return try {
				if (storages.isNotEmpty()) {
					plr.connectStorage(storages[0])
				} else {
					plr.newStorage(Storage.Type.byName(storageType))
				}.also{ storage ->
					if (! storage.postPassphrase(passphrase ?: getPassphraseEntered())) {
						error(res.format("not_authenticated", plrId))
					}
				}
			} catch (e: PLR.StorageNeedPassphraseException) {
				e.postPassphrase(passphrase ?: getPassphraseEntered())
			}
		}

		fun loadJsonMultiMap(reader: Reader): Map<String, List<String>> {
			val tree = ObjectMapper().readTree(reader)
			return tree.fieldNames().asSequence().map { f -> f to tree.get(f).map{it.asText()}}.toMap()
		}

		/**
		 * Get properties from file and System.
		 * When file does not exist, create it with defaults.
		 */
		fun findConfigOrCreate(file: File, defaults: Map<String, String?>): Properties {
			return if (file.exists()) {
				Properties().also{ props ->
					props.load(file.reader())
					defaults.forEach{ (k, v) ->
						val vv = v ?: System.getProperty(k)
						if (props.getProperty(k) == null && vv != null) {
							props.setProperty(k, vv)
						}
					}
				}
			} else {
				Properties().also { props ->
					defaults.forEach{ (k, v) ->
						val vv = v ?: System.getProperty(k)
						if (props.getProperty(k) == null && vv != null) {
							props.setProperty(k, vv)
						}
					}
					props.store(file.writer(), "Configuration for '${Util::class.java.getPackage()}'")
				}
			}
		}
	}

}


data class TypeDef(val type: String) {
	val typeName = type.split(Regex("[^\\w\\.\\-]"), 2)[0]
}

data class LinkDef(val link: String, val endType: String?) {
	val linkName = link.split(Regex("[^\\w\\.\\-]"), 2)[0]
	fun hasValidLinkName(): Boolean = DefLine.validatePropertyName(linkName)
	fun hasValidEndType(): Boolean =
			if (endType != null) DefLine.validateTypeName(endType)
			else false
}

data class DbRecord(
	val table: String,
	val columns: Map<String, TypedLiteral>
)

class Res(private val bundle: ResourceBundle = try {
	ResourceBundle.getBundle("messages", XMLResourceBundleControl())
} catch (e: MissingResourceException) {
	ResourceBundle.getBundle("messages", Locale.ROOT, XMLResourceBundleControl())
}) {

	fun format(messageId: String, vararg args: Any): String {
		return if (bundle.containsKey(messageId)) {
			MessageFormat(bundle.getString(messageId)).format(args)
		} else {
			System.err.println("resource not found: $messageId")
			"$messageId: " + args.joinToString(", ")
		}
	}
}
