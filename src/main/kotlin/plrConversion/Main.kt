package plrConversion

class Main {
	companion object {
		fun showHelp() {
			println("""
	Sub commands:
		push - 
		pull - 
		help - 
		""".trimIndent())
			TODO()
		}

		@JvmStatic
		fun main(args: Array<String>) {
			val options = args.take(1).toTypedArray()
			when (val it = args[0]) {
				"push" -> PushToPlr.main(options)
				"pull" -> PullFromPlr.main(options)
				"debug" -> Debug.main(options)
				"help" -> showHelp()
				else -> {
					System.err.println("subcommand '$it' not found")
					showHelp()
				}
			}
		}
	}
}
