import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.Row
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import plrConversion.*

internal class DefLineTest {

	fun mockRow(a: Array<Any>): Row {
		return mockk<Row> {
			val slot = slot<Int>()
			every { getCell(capture(slot)) } returns mockk {
				every { cellType } answers { when (a[slot.captured]) {
					is Int -> CellType.NUMERIC
					else -> CellType.STRING
				}}
				every { stringCellValue } answers { a[slot.captured].toString() }
			}
			every { lastCellNum } returns (a.size - 1).toShort()
		}
	}

	private fun mockRowListOfStrings(rows: List<List<String?>>): List<Row> = rows.map { row ->
		mockk<Row> {
			every { iterator() } returns row
					.mapIndexed {i, v -> i to v}
					.filter {(_,v) -> v != null}
					.map { (i, v) -> mockk<Cell> {
						every { cellType } returns CellType.STRING
						every { stringCellValue } returns v
						every { columnIndex } returns i
					} }.toMutableList().listIterator()
			every { lastCellNum } returns row.size.toShort()
		}
	}

	@Test
	fun `parse table with heading`() {
		val rows = mockRowListOfStrings(listOf(
				listOf("H", "table", "column", "", "]O Ontology1"),
				listOf("K", "TABLE1", "COL1", ""),
				listOf("B", "", "COL2", "", "Node1","prop1", "integer"),
				listOf("", "", "", "", "comment"),
				listOf("R", "", "", "", "]R Rest1"),
				listOf("R", "", "", "", "Node1 h")))

		val res = DefTable.parseTableWithHeading(mockk {
			every { iterator() } returns rows.toMutableList().iterator()
		})
		assertEquals(DefTable(listOf(
				DefLine(DefFlag.USER_ID, "TABLE1", "COL1", listOf(), null, null, listOf()),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL2", listOf(TypeDef("Node1")), null, null, listOf(LinkDef("prop1", "integer"))))),
				res)
	}

	@Test
	fun `parse table with inner nodes`() {
		val rows = mockRowListOfStrings(listOf(listOf("H", "table", "column", "", "]O"),
				listOf("K", "TABLE1", "COL1", ""),
				listOf("B", "", "NAME1", "", "Node1","prop1", "Name", "first", "string"),
				listOf("B", "", "NAME2", "", "Node1","prop1", "Name", "family", "string"),
				listOf("B", "", "COL3", "", "Node1","prop2", "string")))

		val res = DefTable.parseTableWithHeading(mockk {
			every { iterator() } returns rows.toMutableList().iterator()
		})
		assertEquals(DefTable(listOf(
				DefLine(DefFlag.USER_ID, "TABLE1", "COL1", listOf(), null, null, listOf()),
				DefLine(DefFlag.MAPPING, "TABLE1", "NAME1", listOf(TypeDef("Node1")), null, null, listOf(LinkDef("prop1", "Name"), LinkDef("first", "string"))),
				DefLine(DefFlag.MAPPING, "TABLE1", "NAME2", listOf(TypeDef("Node1")), null, null, listOf(LinkDef("prop1", "Name"), LinkDef("family", "string"))),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL3", listOf(TypeDef("Node1")), null, null, listOf(LinkDef("prop2", "string"))))),
				res)
	}

	@Test
	fun `parse table with shared node definitions`() {
		val rows = mockRowListOfStrings(listOf(listOf("H", "table", "column", "", "]O"),
				listOf("K", "TABLE1", "COL1", ""),
				listOf("B", "", "COL2", "", "Node1 ja:日本語","prop1", "integer"),
				listOf("B", "", "COL3", "", "Node1","prop2", "string")))

		val res = DefTable.parseTableWithHeading(mockk {
			every { iterator() } returns rows.toMutableList().iterator()
		})
		assertEquals(DefTable(listOf(
				DefLine(DefFlag.USER_ID, "TABLE1", "COL1", listOf(), null, null, listOf()),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL2", listOf(TypeDef("Node1 ja:日本語")), null, null, listOf(LinkDef("prop1", "integer"))),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL3", listOf(TypeDef("Node1 ja:日本語")), null, null, listOf(LinkDef("prop2", "string"))))),
				res)
	}

	@Test
	fun `parse 1 line`() {
		val p = DefTable.parseTable(listOf(listOf(
				"B", "TABLE1", "COL1", "foobar", "foobar",
				"Entity", "Person", "name", "string", "", "")), 1, 2, 5)
		assertEquals(DefTable(listOf(
				DefLine(DefFlag.MAPPING, "TABLE1", "COL1",
						listOf(TypeDef("Entity"), TypeDef("Person")), null, null,
						listOf(LinkDef("name", "string"))))), p)
	}

	@Test
	fun `convert rows to lists`() {
		assertEquals(listOf(listOf("10", "20", "30"), listOf("4", "5", "6")),
				DefTable.rowsToIndexedList(mockRowListOfStrings(listOf(listOf("10", "20", "30"), listOf("4", "5", "6")))))
	}

	@Test
	fun `normalize - two column`() {
		val mocks = listOf(listOf("foo", "bar"), listOf("", "bar bar"))
		val p = DefTable.fillEmptyCellsWithAbove(mocks, 0, null)
		assertEquals(listOf(listOf("foo", "bar"), listOf("foo", "bar bar")), p)
	}

	@Test
	fun `normalize - three column`() {
		val mocks = listOf(
				listOf("A", "B", "0"),
				listOf("",  "C", ""),
				listOf("D", "",  ""),
				listOf("" , "E", ""),
				listOf("" , "",  ""),
				listOf("" , "", "F"))
		val p = DefTable.fillEmptyCellsWithAbove(mocks, 0, null)
		assertEquals(listOf(
				listOf("A", "B", "0"),
				listOf("A", "C", ""),
				listOf("D", "",  ""),
				listOf("D", "E", ""),
				listOf("D", "E", ""),
				listOf("D", "E", "F")), p)
	}

	@Test
	fun `normalize - three column - last cells shouldn't join`() {
		val mocks = listOf(
				listOf("A", "B", ""),
				listOf("",  "C", "0"),
				listOf("D", "",  ""),
				listOf("" , "E", "1"),
				listOf("" , "",  ""),
				listOf("" , "", "F"))
		val p = DefTable.fillEmptyCellsWithAbove(mocks, 0, null)
		assertEquals(listOf(
				listOf("A", "B", ""),
				listOf("A", "C", "0"),
				listOf("D", "",  ""),
				listOf("D", "E", "1"),
				listOf("D", "E", ""),
				listOf("D", "E", "F")), p)
	}

	@Test
	fun `normalize - three column - first should be ignored`() {
		val mocks = listOf(
				listOf("A", "B", ""),
				listOf("X",  "C", "0"),
				listOf("D", "",  ""),
				listOf("X" , "E", "1"),
				listOf("" , "",  ""),
				listOf("X" , "", "F"))
		val p = DefTable.fillEmptyCellsWithAbove(mocks, 1, null)
		assertEquals(listOf(
				listOf("A", "B", ""),
				listOf("X", "C", "0"),
				listOf("D", "C", ""),
				listOf("X", "E", "1"),
				listOf("",  "E", ""),
				listOf("X", "E", "F")), p)
	}

	@Test
	fun `normalize - three column - first only`() {
		val mocks = listOf(
				listOf("A", "B", ""),
				listOf("X", "C", "0"),
				listOf("D", "",  ""),
				listOf("X", "E", "1"),
				listOf("" , "",  ""),
				listOf("" , "", "F"))
		val p = DefTable.fillEmptyCellsWithAbove(mocks, 0, 1)
		assertEquals(listOf(
				listOf("A", "B", ""),
				listOf("X", "C", "0"),
				listOf("D", "", ""),
				listOf("X", "E", "1"),
				listOf("X", "", ""),
				listOf("X", "", "F")), p)
	}
}
