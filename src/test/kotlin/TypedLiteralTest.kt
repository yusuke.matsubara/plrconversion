import org.joda.time.DateTime
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import plrConversion.TypedLiteral

class TypedLiteralTest {
	@Test fun `parse integer`() {
		assertEquals(TypedLiteral.of(123), TypedLiteral.guessAndParse("123"))
	}

	@Test fun `parse local time`() {
		assertAll(
				{ assertEquals(DateTime.parse("2019-10-11T12:34:56.000").toString(),
						TypedLiteral.guessAndParse("2019-10-11T12:34:56").value) }
		)
	}
	@Test fun `parse datetime`() {
		assertAll(
				{ assertEquals(TypedLiteral.of(DateTime.parse("2019-10-11")), TypedLiteral.guessAndParse("2019-10-11")) },
				{ assertEquals(TypedLiteral.of(DateTime.parse("2019-10-11T12:34:56")), TypedLiteral.guessAndParse("2019-10-11T12:34:56")) }
		)
	}

	@Test fun `parse not datetime`() {
		assertAll(
				{ assertEquals(TypedLiteral.of("15-10"), TypedLiteral.guessAndParse("15-10")) }
		)
	}
}
