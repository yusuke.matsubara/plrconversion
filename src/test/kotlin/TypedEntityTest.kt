import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import plrConversion.LinkDef
import plrConversion.LiteralType
import plrConversion.TypedEntity
import plrConversion.TypedLiteral

internal class TypedEntityTest {

	@Test
	fun `replacedEntity - depth 1`() {
		val entity = TypedEntity("Person", mapOf(
				"weight" to TypedLiteral(LiteralType.FLOAT, "50.2"),
				"height" to TypedLiteral(LiteralType.INT, "100")))
		assertEquals(
				TypedEntity("Person", mapOf(
						"weight" to TypedLiteral(LiteralType.FLOAT, "50.2"),
						"height" to TypedLiteral(LiteralType.INT, "100"),
						"address" to TypedLiteral(LiteralType.STR, "Tokyo"))),
				entity.replacedEntity(listOf(LinkDef("address", "Address")), TypedLiteral(LiteralType.STR, "Tokyo"))
		)
	}

	@Test
	fun `replacedEntity - depth 2`() {
		val entity = TypedEntity("Person", mapOf(
				"weight" to TypedLiteral(LiteralType.FLOAT, "50.2"),
				"height" to TypedLiteral(LiteralType.INT, "100")))
		assertEquals(
				TypedEntity("Person", mapOf(
						"weight" to TypedLiteral(LiteralType.FLOAT, "50.2"),
						"height" to TypedLiteral(LiteralType.INT, "100"),
						"address" to TypedEntity("Address", mapOf("city" to TypedLiteral(LiteralType.STR, "Tokyo"))))),
				entity.replacedEntity(listOf(
						LinkDef("address", "Address"),
						LinkDef("city", null)),
						TypedLiteral(LiteralType.STR, "Tokyo"))
		)
	}
}
