import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import plrConversion.Res
import java.util.*

class ResTest {
	fun mockResourceBundle(map: Map<String,String>): ResourceBundle {
		val slot = slot<String>()
		return mockk {
			every { containsKey(capture(slot))} answers { map.containsKey(slot.captured) }
			every { getString(capture(slot)) } answers { map[slot.captured]!! }
		}
	}
	fun mockSingle(s: String): ResourceBundle {
		return mockk {
			every { containsKey(any()) } returns true
			every { getString(any()) } returns s
		}
	}

	@Test
	fun `resource not found`() {
		assertEquals("test_string: A, BC",
				Res(mockResourceBundle(mapOf()))
						.format("test_string", "A", "BC"))
	}

	@Test
	fun `format with 1 argument`() {
		assertEquals("foo ABC bar",
				Res(mockSingle("foo {0} bar"))
						.format("X", "ABC"))
	}

	@Test
	fun `format with incomplete argument`() {
		assertEquals("foo ABC bar {1}",
				Res(mockSingle("foo {0} bar {1}"))
						.format("X", "ABC"))
	}

	@Test
	fun `reuse res instance`() {
		val res = Res(mockResourceBundle(mapOf("text1" to "foo {0} bar", "text2" to "bar {0} bar")))
		assertEquals("foo ABC bar", res.format("text1", "ABC"))
		assertEquals("bar DEF bar", res.format("text2", "DEF"))
	}
}