import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import plrConversion.Util
import java.time.Instant

class UtilTest {
	@Test
	fun `parse relative datetime`() {
		assertAll(
				{ assertEquals(Instant.parse("2015-01-03T01:23:00Z"),
						Util.parseDateTimeAbsoluteOrRelative("P1D", Instant.parse("2015-01-02T01:23:00Z"))) },
				{ assertEquals(Instant.parse("2015-01-01T23:23:00Z"),
						Util.parseDateTimeAbsoluteOrRelative("PT-2H", Instant.parse("2015-01-02T01:23:00Z"))) })
	}
	@Test
	fun `parse absolute datetime`() {
		assertAll(
				{ assertEquals(Instant.parse("2015-01-03T01:23:00Z"),
						Util.parseDateTimeAbsoluteOrRelative("2015-01-03T01:23:00Z")) })
	}

	@Test
	fun `load json`() {
		assertEquals(mapOf("foo" to listOf("bar", "barbar"), "foo2" to listOf("bar")), Util.loadJsonMultiMap("""
			{"foo": ["bar", "barbar"],
			"foo2": ["bar"]}
		""".trimIndent().reader()))
	}
}