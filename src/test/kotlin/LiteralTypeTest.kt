import com.assemblogue.plr.lib.LiteralNode
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import plrConversion.LiteralType

internal class LiteralTypeTest {
	@Test
	fun `parse integer type`() {
		assertEquals(LiteralType.INT, LiteralType.of(mockk<LiteralNode> {
			every { type } returns listOf("int")
		}))
	}
}