import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.assertAll
import plrConversion.FriendList
import java.io.StringWriter

internal class FriendListTest {

	@Test
	fun `load list without timestamps`() {
		val list = FriendList.load("""
			googleDrive:zzz@example.com,AAAA,2019-12-05T06:13:03.796Z
			googleDrive:ali@example.com,U001
			googleDrive:bob@example.com,U002
		""".trimIndent().reader())
		assertAll(
				{ assertEquals(setOf("googleDrive:ali@example.com", "googleDrive:bob@example.com", "googleDrive:zzz@example.com"),
						list.allPlrIds().toSet()) },
				{ assertEquals("googleDrive:ali@example.com",
						list.findFromDbUserId("U001")!!.plrId) })
	}
	@Test
	fun `save and load`() {
		val list = FriendList()
		list.add("googleDrive:ali@example.com", "U001")
		list.add("googleDrive:bob@example.com", "U002")
		list.add("googleDrive:zzz@example.com", "AAAA")
		val writer = StringWriter()
		list.saveToFile(writer)
		println(writer.toString())
		val loaded = FriendList.load(writer.toString().reader())
		assertAll(
				{ assertEquals(list.allPlrIds(), loaded.allPlrIds()) },
				{ assertEquals("googleDrive:ali@example.com",
						loaded.findFromDbUserId("U001")!!.plrId) })
	}
}