import plrConversion.TypedLiteral.Companion.of
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import plrConversion.DbTable
import java.io.StringWriter

internal class DbTableTest {

	fun asPrinted(s: String) = s.trimIndent().replace("\n", System.lineSeparator())

	@Test
	fun `addRow 1`() {
		val db = DbTable("myTable")
		db.addRow(mapOf("col1" to of(10), "col2" to of(20)))
		db.addRow(mapOf("col2" to of(100), "col3" to of("ABC")))

		assertEquals(listOf(
				listOf(of(10), of(20), null),
				listOf(null, of(100), of("ABC"))
		), db.getRowsAsLists())
	}

	@Test
	fun `from csv - full table`() {
		val db = DbTable.fromHeaderedCsv("myTable", """
			col1,col2,col3
			a,b,c
			d,e,9
		""".trimIndent().reader())
		assertEquals(DbTable("myTable")
				.addRow(mapOf("col1" to of("a"), "col2" to of("b"), "col3" to of("c")))
				.addRow(mapOf("col1" to of("d"), "col2" to of("e"), "col3" to of(9))), db)
	}

	@Test
	fun `from csv - missing values`() {
		val db = DbTable.fromHeaderedCsv("myTable", """
			col1,col2,col3
			a,,c
			d,e,f
		""".trimIndent().reader())
		assertEquals(DbTable("myTable")
				.addRow(mapOf("col1" to of("a"), "col2" to of(""), "col3" to of("c")))
				.addRow(mapOf("col1" to of("d"), "col2" to of("e"), "col3" to of("f"))), db)
	}

	@Test
	fun `to csv - reverse`() {
		val source =  """
			col1,col2,col3
			a,,c
			d,e,f
		""".trimIndent()
		val db = DbTable.fromHeaderedCsv("myTable", source.reader())
		val writer = StringWriter()
		db.toHeaderedCsv(writer)
		assertEquals(asPrinted(source), writer.toString().trim())
	}

	@Test
	fun `reading with ignored columns`() {
		val source =  """
			col1,col2,col3
			a,,c
			d,e,f
		""".trimIndent()
		val db = DbTable.fromHeaderedCsv("myTable", source.reader(), DbTable.Filter(setOf("col2", "col4")))
		val writer = StringWriter()
		db.toHeaderedCsv(writer)
		assertEquals(asPrinted("""
			col1,col3
			a,c
			d,f
		"""), writer.toString().trim())
	}

	@Test
	fun `writing with ignored columns`() {
		val source =  """
			col1,col2,col3
			a,,c
			d,e,f
		""".trimIndent()
		val db = DbTable.fromHeaderedCsv("myTable", source.reader())
		val writer = StringWriter()
		db.toHeaderedCsv(writer, DbTable.Filter(setOf("col2", "col3", "col4")))
		assertEquals(asPrinted("""
			col1
			a
			d
		"""), writer.toString().trim())
	}

	@Test
	fun `to csv - defined`() {
		val source =  """
			col1,col2,col3
			a,,c
			d,e,f
		""".trimIndent()
		val db = DbTable.fromHeaderedCsv("myTable", source.reader(), DbTable.Filter(setOf("col2", "col4")))
		val writer = StringWriter()
		db.toHeaderedCsv(writer)
		assertEquals(asPrinted("""
			col1,col3
			a,c
			d,f
		"""), writer.toString().trim())
	}

	@Test
	fun `shrink complementary rows`() {
		val db = DbTable("myTable")
		db.addRow(mapOf("col1" to of(0), "col2" to of(0)))
		db.addRow(mapOf("col1" to of(10), "col2" to of(20)))
		db.addRow(mapOf("col3" to of("ABC")))
		db.addRow(mapOf("col2" to of(20), "col3" to of("ABC")))
		db.addRow(mapOf("col2" to of(100), "col3" to of("ABC")))
		assertEquals(DbTable("myTable")
				.addRow(mapOf("col1" to of(0), "col2" to of(0)))
				.addRow(mapOf("col1" to of(10), "col2" to of(20), "col3" to of("ABC")))
				.addRow(mapOf("col2" to of(100), "col3" to of("ABC"))), db.shrink())
	}
}
