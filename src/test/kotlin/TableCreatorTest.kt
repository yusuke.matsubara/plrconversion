import com.assemblogue.plr.lib.EntityNode
import com.assemblogue.plr.lib.FileNode
import com.assemblogue.plr.lib.LiteralNode
import com.assemblogue.plr.lib.Node
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.api.Test
import plrConversion.*

class TableCreatorTest {

	private fun mockLiteralNode(type: String, value: String) = mockk<LiteralNode> {
		every { isLiteral() } returns true
		every { asLiteral() } returns this
		every { getType() } returns listOf(type)
		every { getValue() } returns value
	}

	private fun mockFileNode(format: String, value: ByteArray) = mockk<FileNode> {
		every { isLiteral() } returns false
		every { isFile() } returns true
		every { asFile() } returns this
		every { getFormat() } returns format
		every { get() } returns value.inputStream()
	}

	private fun mockTimelineItem(type:String, props: Map<String, List<Node>>, begin: DateTime? = null): EntityNode {
		val item = mockk<EntityNode> {
			every { isEntity() } returns true
			every { asEntity() } returns this
			every { getBegin() } returns listOf(begin?.toDate())
			every { getType() } returns listOf(type)

			val prop = slot<String>()
			every { hasProperty(capture(prop)) } answers { props.contains(prop.captured) }
		}
		props.forEach { (p, nodes) ->
			every { item invoke "getProperty" withArguments listOf(p) } returns nodes
		}

		return item
	}

	@Test
	fun `parse 1 item with 2 properties into 1 table`() {
		val creator = TableCreator(DefTable(listOf(
				DefLine(DefFlag.MAPPING, "TABLE1", "COL1", listOf(TypeDef("N1")), "integer", "",
						listOf(LinkDef("p1", null))),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL2", listOf(TypeDef("N1")), "string", "",
						listOf(LinkDef("p2", null))),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL3", listOf(TypeDef("N1")), "string", "",
						listOf(LinkDef("p3", null))))),
				null)
		val items = mapOf("user_x" to
				listOf(mockTimelineItem("N1", mapOf(
						"p1" to listOf(mockLiteralNode("Integer", "1000")),
						"p3" to listOf(mockLiteralNode("String", "ABC"))))))

		val tables = creator.createTables(items)
		assertAll(
				{ assertEquals(3, creator.defs.lines.size) },
				{ assertEquals(listOf("COL1", "COL3"), tables.getValue("TABLE1").getColumns()) },
				{ assertEquals(listOf(listOf(TypedLiteral.of(1000), TypedLiteral.of("ABC"))),
						tables.getValue("TABLE1").getRowsAsLists())}
		)
	}

	@Test
	fun `parse begin`() {
		val creator = TableCreator(DefTable(listOf(
				DefLine(DefFlag.MAPPING, "TABLE1", "COL1", listOf(TypeDef("N1")), "integer", "",
						listOf(LinkDef("p1", null))),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL2", listOf(TypeDef("N1")), "datetime", "",
						listOf(LinkDef("begin", null))),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL3", listOf(TypeDef("N1")), "string", "",
						listOf(LinkDef("p3", null))))),
				null)
		val date1 = DateTime.parse("2019-11-01T12:34:56Z").toDateTime(DateTimeZone.getDefault())
		val items = mapOf("user_x" to
				listOf(mockTimelineItem("N1", mapOf(
						"p1" to listOf(mockLiteralNode("Integer", "1000")),
						"p3" to listOf(mockLiteralNode("String", "ABC"))), date1)))

		val tables = creator.createTables(items)
		assertAll(
				{ assertEquals(listOf("COL1", "COL2", "COL3"), tables.getValue("TABLE1").getColumns()) },
				{ assertEquals(listOf(listOf(TypedLiteral.of(1000), TypedLiteral.of(date1), TypedLiteral.of("ABC"))),
						tables.getValue("TABLE1").getRowsAsLists())}
		)
	}


	@Test
	fun `count mmdata`() {
		val creator = TableCreator(DefTable(listOf(
				DefLine(DefFlag.MAPPING, "TABLE1", "COL1", listOf(TypeDef("N1")), null, "",
						listOf(LinkDef("p1", "integer"))),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL2", listOf(TypeDef("N1")), null, "",
						listOf(LinkDef("p2", "string"))),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL3", listOf(TypeDef("N1")), null, "",
						listOf(LinkDef("p3", "mmdata"))))),
				null)
		val items = mapOf("user_x" to
				listOf(mockTimelineItem("N1", mapOf(
						"p1" to listOf(mockLiteralNode("Integer", "200")),
						"p3" to listOf(
								mockFileNode("text/plain", "foobar".toByteArray()),
								mockFileNode("text/plain", "bar".toByteArray()))))))

		val tables = creator.createTables(items)
		assertAll(
				{ assertEquals(listOf("COL1", "COL3"), tables.getValue("TABLE1").getColumns()) },
				{ assertEquals(listOf(listOf(TypedLiteral.of(200), TypedLiteral.of(2))),
						tables.getValue("TABLE1").getRowsAsLists())}
		)
	}

	@Test
	fun `parse 2 items into 1 table - same type, 2 rows`() {
		val creator = TableCreator(DefTable(listOf(
				DefLine(DefFlag.MAPPING, "TABLE1", "COL1", listOf(TypeDef("N1")), "integer", "",
						listOf(LinkDef("p1", null))),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL2", listOf(TypeDef("N1")), "string", "",
						listOf(LinkDef("p2", null))),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL3", listOf(TypeDef("N1")), "string", "",
						listOf(LinkDef("p3", null))))),
				null)
		val items = mapOf("user_x" to listOf(
				mockTimelineItem("N1", mapOf(
						"p1" to listOf(mockLiteralNode("Integer", "1000")),
						"p3" to listOf(mockLiteralNode("String", "ABC")))),
				mockTimelineItem("N1", mapOf(
						"p2" to listOf(mockLiteralNode("String", "foo")),
						"p3" to listOf(mockLiteralNode("String", "bar"))))))

		val tables = creator.createTables(items)
		assertAll(
				{ assertEquals(3, creator.defs.lines.size) },
				{ assertEquals(listOf("COL1", "COL2", "COL3"), tables.getValue("TABLE1").getColumns()) },
				{ assertEquals(listOf(listOf(TypedLiteral.of(1000), null, TypedLiteral.of("ABC")),
						listOf(null, TypedLiteral.of("foo"), TypedLiteral.of("bar"))),
						tables.getValue("TABLE1").getRowsAsLists())}
		)
	}

	@Test
	fun `parse 2 items into 1 table`() {
		val creator = TableCreator(DefTable(listOf(
				DefLine(DefFlag.MAPPING, "TABLE1", "COL1", listOf(TypeDef("N1")), "integer", "",
						listOf(LinkDef("p1", null))),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL2", listOf(TypeDef("N1")), "string", "",
						listOf(LinkDef("p2", null))),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL3", listOf(TypeDef("N1")), "string", "",
						listOf(LinkDef("p3", null))),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL4", listOf(TypeDef("N2")), "integer", "",
						listOf(LinkDef("p4", null))),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL5", listOf(TypeDef("N2")), "string", "",
						listOf(LinkDef("p5", null))))),
				null)
		val items = mapOf("user_x" to listOf(
				mockTimelineItem("N1", mapOf(
						"p1" to listOf(mockLiteralNode("Integer", "1000")),
						"p3" to listOf(mockLiteralNode("String", "ABC")))),
				mockTimelineItem("N2", mapOf(
						"p5" to listOf(mockLiteralNode("String", "bar"))))))

		val tables = creator.createTables(items)
		assertAll(
				{ assertEquals(5, creator.defs.lines.size) },
				{ assertEquals(listOf("COL1", "COL3", "COL5"), tables.getValue("TABLE1").getColumns()) },
				{ assertEquals(listOf(
						listOf(TypedLiteral.of(1000), TypedLiteral.of("ABC"), null),
						listOf(null, null, TypedLiteral.of("bar"))),
						tables.getValue("TABLE1").getRowsAsLists())}
		)
	}

	@Test
	fun `parse 3 items into 2 table`() {
		val creator = TableCreator(DefTable(listOf(
				DefLine(DefFlag.MAPPING, "TABLE1", "COL1", listOf(TypeDef("N1")), "integer", "",
						listOf(LinkDef("p1", null))),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL2", listOf(TypeDef("N1")), "string", "",
						listOf(LinkDef("p2", null))),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL3", listOf(TypeDef("N1")), "string", "",
						listOf(LinkDef("p3", null))),
				DefLine(DefFlag.MAPPING, "TABLE2", "COL4", listOf(TypeDef("N2")), "integer", "",
						listOf(LinkDef("p4", null))),
				DefLine(DefFlag.MAPPING, "TABLE2", "COL5", listOf(TypeDef("N2")), "string", "",
						listOf(LinkDef("p5", null))))),
				null)
		val items = mapOf("user_x" to listOf(
				mockTimelineItem("N1", mapOf(
						"p1" to listOf(mockLiteralNode("Integer", "1000")))),
				mockTimelineItem("N2", mapOf(
						"p5" to listOf(mockLiteralNode("String", "bar")))),
				mockTimelineItem("N1", mapOf(
						"p1" to listOf(mockLiteralNode("Integer", "2000")),
						"p3" to listOf(mockLiteralNode("String", "ABC"))))))

		val tables = creator.createTables(items)
		assertAll(
				{ assertEquals(listOf("COL1", "COL3"), tables.getValue("TABLE1").getColumns()) },
				{ assertEquals(listOf("COL5"), tables.getValue("TABLE2").getColumns()) },
				{ assertEquals(listOf(
						listOf(TypedLiteral.of(1000), null),
						listOf(TypedLiteral.of(2000), TypedLiteral.of("ABC"))),
						tables.getValue("TABLE1").getRowsAsLists()) },
				{ assertEquals(
						listOf(listOf(TypedLiteral.of("bar"))),
						tables.getValue("TABLE2").getRowsAsLists()) }
		)
	}

	@Test
	fun `parse 3 items into 1 table with 2 users`() {
		val creator = TableCreator(DefTable(listOf(
				DefLine(DefFlag.MAPPING, "TABLE1", "COL1", listOf(TypeDef("N1")), "integer", "",
						listOf(LinkDef("p1", null))),
				DefLine(DefFlag.USER_ID, "TABLE1", "COL2", listOf(), null, "", listOf()),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL3", listOf(TypeDef("N1")), "string", "",
						listOf(LinkDef("p3", null))),
				DefLine(DefFlag.USER_ID, "TABLE2", "COL4", listOf(), null, "", listOf()),
				DefLine(DefFlag.MAPPING, "TABLE2", "COL5", listOf(TypeDef("N2")), "string", "",
						listOf(LinkDef("p5", null))))),
				null)
		val items = mapOf(
				"user1" to listOf(
						mockTimelineItem("N1", mapOf(
								"p1" to listOf(mockLiteralNode("Integer", "1000"))))),
				"user2" to listOf(
						mockTimelineItem("N2", mapOf(
								"p5" to listOf(mockLiteralNode("String", "bar")))),
						mockTimelineItem("N1", mapOf(
								"p1" to listOf(mockLiteralNode("Integer", "2000")),
								"p3" to listOf(mockLiteralNode("String", "ABC"))))))

		val tables = creator.createTables(items)
		assertAll(
				{ assertEquals(listOf("COL1", "COL2", "COL3"), tables.getValue("TABLE1").getColumns()) },
				{ assertEquals(listOf("COL4", "COL5"), tables.getValue("TABLE2").getColumns()) },
				{ assertEquals(
						listOf(
								listOf(TypedLiteral.of(1000), TypedLiteral.of("user1"), null),
								listOf(TypedLiteral.of(2000), TypedLiteral.of("user2"), TypedLiteral.of("ABC"))),
						tables.getValue("TABLE1").getRowsAsLists()) },
				{ assertEquals(
						listOf(listOf(TypedLiteral.of("user2"), TypedLiteral.of("bar"))),
						tables.getValue("TABLE2").getRowsAsLists()) }
		)
	}
}

