import com.assemblogue.plr.lib.EntityNode
import io.mockk.*
import org.joda.time.DateTime
import org.joda.time.Duration
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import plrConversion.*

class NodeCreatorTest {

	fun createNodes(creator: NodeCreator, parent: EntityNode, records: List<DbRecord>): List<EntityNode> {
		return creator.prepareNodes(records).map { e ->
			creator.createNode(parent, e)
		}.toList()
	}

	@Test
	fun `divide`() {
		assertEquals(listOf(
				TypeDef("N1 ja:XXXX") to listOf(
						DefLine(DefFlag.MAPPING, "TABLE2", "COL2", listOf(TypeDef("N1 ja:XXXX")), "integer", "",
								listOf(LinkDef("p1", null))),
						DefLine(DefFlag.MAPPING, "TABLE1", "COL1", listOf(TypeDef("N1 ja:XXXX")), "integer", "",
								listOf(LinkDef("p3", null)))),
				TypeDef("N2") to listOf(
						DefLine(DefFlag.MAPPING, "TABLE3", "COL3", listOf(TypeDef("N2")), "integer", "",
								listOf(LinkDef("p2", null))),
						DefLine(DefFlag.MAPPING, "TABLE2", "COL3", listOf(TypeDef("N2")), "integer", "",
								listOf(LinkDef("p4", null))))),
				NodeCreator.divideDefsByNode(listOf(
						DefLine(DefFlag.USER_ID, "TABLE2", "ID", listOf(), "integer", "",
								listOf()),
						DefLine(DefFlag.MAPPING, "TABLE2", "COL2", listOf(TypeDef("N1 ja:XXXX")), "integer", "",
								listOf(LinkDef("p1", null))),
						DefLine(DefFlag.MAPPING, "TABLE1", "COL1", listOf(TypeDef("N1 ja:XXXX")), "integer", "",
								listOf(LinkDef("p3", null))),
						DefLine(DefFlag.MAPPING, "TABLE3", "COL3", listOf(TypeDef("N2")), "integer", "",
								listOf(LinkDef("p2", null))),
						DefLine(DefFlag.MAPPING, "TABLE2", "COL3", listOf(TypeDef("N2")), "integer", "",
								listOf(LinkDef("p4", null))))))
	}

	@Test
	fun `Create with multiple values`() {
		//TODO()
	}

	@Test
	fun `Create with empty values`() {
		//TODO()
	}

	@Test
	fun `Create from simplified datetime format`() {
		val defs = DefTable(listOf(
				DefLine(DefFlag.USER_ID, "TABLE1", "UID", listOf(TypeDef("N1")), "", "", listOf()),
				DefLine(DefFlag.MAPPING, "TABLE1", "ET", listOf(TypeDef("N1")), "", "",
						listOf(LinkDef("estimatedTime", "datetime"))),
				DefLine(DefFlag.MAPPING, "TABLE1", "timestamp", listOf(TypeDef("N1")), "", "",
						listOf(LinkDef("begin", "datetime")))))
		val creator = NodeCreator(defs.lines, null)

		val tables = listOf(
				DbTable.fromHeaderedCsv("TABLE1", """
					UID,timestamp,ET
					Alice,2019-11-22,
					Alice,2019-1-2,2019-1-1
				""".trimIndent().reader(), defs))
		val entities = creator.convertDbTablesIntoRecords(tables).flatMap { (_, records) ->
			creator.prepareNodes(records).toList()
		}
		assertEquals(listOf(
				TypedEntity("N1", mapOf("begin" to TypedLiteral(LiteralType.DATETIME, "2019-11-22"))),
				TypedEntity("N1", mapOf(
						"begin" to TypedLiteral(LiteralType.DATETIME, "2019-1-2"),
						"estimatedTime" to TypedLiteral(LiteralType.DATETIME, "2019-1-1")))),
				entities)

		mockk<EntityNode>(relaxed = true) {
			every { storage.type.name } returns "googleDrive"
			every { storage.userId } returns "FooBar@gmail.com"
		}.also { mock ->
			val items = entities.map{ creator.createNode(mock, it) }
			verifyAll {
				items[0].newLiteral("creator").value = "googleDrive:FooBar@gmail.com"
				items[0].setBegin(systemDateFormat("2019-11-22T00:00:00.000"))
				items[1].newLiteral("creator").value = "googleDrive:FooBar@gmail.com"
				items[1].newLiteral("estimatedTime").value = systemDateFormat("2019-01-01T00:00:00.000")
				items[1].setBegin(systemDateFormat("2019-01-02T00:00:00.000"))
			}
		}
	}

	private fun systemDateFormat(s: String) = DateTime.parse(s).toString()

	@Test
	fun `Create 2 flat nodes with direct properties only`() {
		val creator = NodeCreator(listOf(
				DefLine(DefFlag.USER_ID, "TABLE1", "UID", listOf(TypeDef("N1 ja:日本語")), "string", "", listOf()),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL1", listOf(TypeDef("N1")), "integer", "",
						listOf(LinkDef("p1 ja:日本語", "integer"))),
				DefLine(DefFlag.USER_ID, "TABLE2", "UID", listOf(TypeDef("N2")), "string", "", listOf()),
				DefLine(DefFlag.MAPPING, "TABLE2", "COL3", listOf(TypeDef("N2")), "integer", "",
						listOf(LinkDef("p4", "integer"))),
				DefLine(DefFlag.MAPPING, "TABLE2", "COL2", listOf(TypeDef("N2")), "datetime", "",
						listOf(LinkDef("p3=1", "datetime")))), null)

		val date1 = DateTime.parse("2019-10-10T12:00:34Z").toString()
		val entries = listOf(
				DbRecord("TABLEX", mapOf("COLX" to TypedLiteral.of("X"))),
				DbRecord("TABLE1", mapOf(
						"UID" to TypedLiteral.of("Foo"),
						"COL1" to TypedLiteral.of(10))),
				DbRecord("TABLE2", mapOf(
						"UID" to TypedLiteral.of("Foo"),
						"COL3" to TypedLiteral.of(111),
						"COL2" to TypedLiteral(LiteralType.DATETIME, date1))))

		mockk<EntityNode>(relaxed = true) {
			every { storage.type.name } returns "googleDrive"
			every { storage.userId } returns "FooBar@gmail.com"
		}.also { mock ->
			val items = createNodes(creator, mock, entries)
			verifyAll {
				items[0].newLiteral("creator").value = "googleDrive:FooBar@gmail.com"
				items[0].newLiteral("p1").value = 10
				items[1].newLiteral("p4").value = 111
				items[1].newLiteral("creator").value = "googleDrive:FooBar@gmail.com"
				items[1].newLiteral("p3").value = date1
			}
		}
	}

	@Test
	fun `Create 1 flat node, subject-writable`() {
		val creator = NodeCreator(listOf(
				DefLine(DefFlag.USER_ID_SUBJECT_WRITABLE, "TABLE1", "UID", listOf(TypeDef("N1 ja:日本語")), "string", "", listOf()),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL1", listOf(TypeDef("N1 ja:日本語")), "integer", "",
						listOf(LinkDef("p1 ja:日本語", "integer"))),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL4", listOf(TypeDef("N1 ja:日本語")), "integer", "",
						listOf(LinkDef("p4", "integer")))), null)

		val entries = listOf(
				DbRecord("TABLE1", mapOf(
						"COL1" to TypedLiteral.of(10),
						"COL4" to TypedLiteral.of(111))))

		mockk<EntityNode>(relaxed = true) {
			every { storage.type.name } returns "googleDrive"
			every { meta.owner } returns "FooBar@gmail.com"
		}.also { mock ->
			val items = createNodes(creator, mock, entries)
			verifyAll {
				items[0].newLiteral("creator").value = "googleDrive:FooBar@gmail.com"
				items[0].newLiteral("p1").value = 10
				items[0].newLiteral("p4").value = 111
			}
		}
	}

	@Test
	fun `dbtable to records`() {
		val creator = NodeCreator(listOf(
				DefLine(DefFlag.USER_ID, "TABLE1", "USR", listOf(TypeDef("N1")), null, "", listOf()),
				DefLine(DefFlag.USER_ID, "TABLE2", "USR", listOf(TypeDef("N2")), null, "", listOf()),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL1", listOf(TypeDef("N1")), "integer", "", listOf())),
				null)
		val tables = listOf(DbTable("TABLEX"), DbTable("TABLE1"), DbTable("TABLE2"))
		tables[0].addRow(mapOf(
				"USR" to TypedLiteral.of("Alice"),
				"COLX" to TypedLiteral.of("X")))
		tables[1].addRow(mapOf(
				"USR" to TypedLiteral.of("Alice"),
				"COL1" to TypedLiteral.of(10)))
		tables[2].addRow(mapOf(
				"USR" to TypedLiteral.of("Alice"),
				"COL3" to TypedLiteral.of(111),
				"COL2" to TypedLiteral.of("AAA")))
		tables[2].addRow(mapOf(
				"USR" to TypedLiteral.of("Bob"),
				"COL3" to TypedLiteral.of(222),
				"COL2" to TypedLiteral.of("BBB")))
		assertEquals(listOf(
				"Alice" to listOf(
						DbRecord("TABLE1", mapOf(
								"USR" to TypedLiteral.of("Alice"),
								"COL1" to TypedLiteral.of(10))),
						DbRecord("TABLE2", mapOf(
								"USR" to TypedLiteral.of("Alice"),
								"COL3" to TypedLiteral.of(111),
								"COL2" to TypedLiteral.of("AAA")))),
				"Bob" to listOf(
						DbRecord("TABLE2", mapOf(
								"USR" to TypedLiteral.of("Bob"),
								"COL3" to TypedLiteral.of(222),
								"COL2" to TypedLiteral.of("BBB"))))),
				creator.convertDbTablesIntoRecords(tables))
	}

	@Test
	fun `Create 2 flat nodes for each record`() {
		val creator = NodeCreator(listOf(
				DefLine(DefFlag.USER_ID, "TABLE2", "USR", listOf(TypeDef("N1")), null, "", listOf()),
				DefLine(DefFlag.MAPPING, "TABLE2", "COL3", listOf(TypeDef("N1"), TypeDef("N2")), "integer", "",
						listOf(LinkDef("p3", null))),
				DefLine(DefFlag.MAPPING, "TABLE2", "COL2", listOf(TypeDef("N1"), TypeDef("N3")), "string", "",
						listOf(LinkDef("p2", null)))), null)

		val tables = listOf(DbTable("TABLE2")
				.addRow(mapOf(
						"USR" to TypedLiteral.of("Alice"),
						"COL3" to TypedLiteral.of(111),
						"COL2" to TypedLiteral.of("AAA")))
				.addRow(mapOf(
						"USR" to TypedLiteral.of("Bob"),
						"COL3" to TypedLiteral.of(222),
						"COL2" to TypedLiteral.of("BBB"))))

		val items = creator.convertDbTablesIntoRecords(tables).flatMap { (u, entries) ->
			createNodes(creator, when (u.toUpperCase()) {
				"ALICE" -> mockk(relaxed = true) {
					every { storage.type.name } returns "googleDrive"
					every { storage.userId } returns "Foo@gmail.com"
				}
				"BOB" -> mockk(relaxed = true) {
					every { storage.type.name } returns "googleDrive"
					every { storage.userId } returns "Bar@gmail.com"
				}
				else -> {
					fail("$u not found")
				}
			}, entries)
		}
		assertEquals(4, items.size)
		verifyAll {
			items[0].newLiteral("creator").value = "googleDrive:Foo@gmail.com"
			items[0].newLiteral("p2").value = "AAA"
			items[1].newLiteral("creator").value = "googleDrive:Foo@gmail.com"
			items[1].newLiteral("p3").value = 111
			items[2].newLiteral("creator").value = "googleDrive:Bar@gmail.com"
			items[2].newLiteral("p2").value = "BBB"
			items[3].newLiteral("creator").value = "googleDrive:Bar@gmail.com"
			items[3].newLiteral("p3").value = 222
		}
	}

	@Test
	fun `Create 4 flat nodes for 2 users`() {
		val creator = NodeCreator(listOf(
				DefLine(DefFlag.USER_ID, "TABLE1", "USR", listOf(TypeDef("N1")), null, "", listOf()),
				DefLine(DefFlag.MAPPING, "TABLE1", "DATE", listOf(TypeDef("N1")), "datetime", "",
						listOf(LinkDef("begin", null))),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL1", listOf(TypeDef("N1")), "integer", "",
						listOf(LinkDef("p1", null))),
				DefLine(DefFlag.USER_ID, "TABLE2", "USR", listOf(TypeDef("N2")), null, "", listOf()),
				DefLine(DefFlag.MAPPING, "TABLE2", "COL3", listOf(TypeDef("N2")), "integer", "",
						listOf(LinkDef("p4", null))),
				DefLine(DefFlag.MAPPING, "TABLE2", "COL2", listOf(TypeDef("N2")), "string", "",
						listOf(LinkDef("p3", null)))), null)

		val date1 = DateTime.parse("2019-10-11T12:00:34Z").toString()
		val date2 = DateTime.parse("2019-10-12T12:00:34Z").toString()

		val tables = listOf(DbTable("TABLEX"), DbTable("TABLE1"), DbTable("TABLE2"))
		tables[0].addRow(mapOf(
				"USR" to TypedLiteral.of("Alice"),
				"COLX" to TypedLiteral.of("X")))
		tables[1].addRow(mapOf(
				"USR" to TypedLiteral.of("Alice"),
				"DATE" to TypedLiteral(LiteralType.DATETIME, date1),
				"COL1" to TypedLiteral.of(10)))
		tables[1].addRow(mapOf(
				"USR" to TypedLiteral.of("Alice"),
				"DATE" to TypedLiteral(LiteralType.DATETIME, date2),
				"COL1" to TypedLiteral.of(200)))
		tables[2].addRow(mapOf(
				"USR" to TypedLiteral.of("Alice"),
				"COL3" to TypedLiteral.of(111),
				"COL2" to TypedLiteral.of("AAA")))
		tables[2].addRow(mapOf(
				"USR" to TypedLiteral.of("Bob"),
				"COL3" to TypedLiteral.of(222),
				"COL2" to TypedLiteral.of("BBB")))

		val items = creator.convertDbTablesIntoRecords(tables).flatMap { (u, entries) ->
			createNodes(creator, when (u.toUpperCase()) {
				"ALICE" -> mockk(relaxed = true) {
					every { storage.type.name } returns "googleDrive"
					every { storage.userId } returns "Foo@gmail.com"
				}
				"BOB" -> mockk(relaxed = true) {
					every { storage.type.name } returns "googleDrive"
					every { storage.userId } returns "Bar@gmail.com"
				}
				else -> {
					fail("$u not found")
				}
			}, entries)
		}
		assertEquals(4, items.size)
		verifyAll {
			items[0].newLiteral("creator").value = "googleDrive:Foo@gmail.com"
			items[0].newLiteral("p1").value = 10
			items[0].setBegin(date1)
			items[1].newLiteral("creator").value = "googleDrive:Foo@gmail.com"
			items[1].newLiteral("p1").value = 200
			items[1].setBegin(date2)
			items[2].newLiteral("creator").value = "googleDrive:Foo@gmail.com"
			items[2].newLiteral("p4").value = 111
			items[2].newLiteral("p3").value = "AAA"
			items[3].newLiteral("creator").value = "googleDrive:Bar@gmail.com"
			items[3].newLiteral("p4").value = 222
			items[3].newLiteral("p3").value = "BBB"
		}
	}

	@Test
	fun `Create 1 hierarchical node (depth 1)`() {
		val creator = NodeCreator(listOf(
				DefLine(DefFlag.USER_ID, "TABLE1", "UID", listOf(TypeDef("N1")), "string", "", listOf()),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL1", listOf(TypeDef("N1")), "integer", "",
						listOf(LinkDef("p1", "P1"),
								LinkDef("x1", null))),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL3", listOf(TypeDef("N1")), "string", "",
						listOf(LinkDef("p4", "P2"),
								LinkDef("x2", null)))), null)

		val entries = listOf(
				DbRecord("TABLEX", mapOf("COLX" to TypedLiteral.of("X"))),
				DbRecord("TABLE1", mapOf(
						"COL3" to TypedLiteral.of("AAA"),
						"COL1" to TypedLiteral.of(111))))

		mockk<EntityNode>(relaxed = true) {
			every { storage.type.name } returns "googleDrive"
			every { storage.userId } returns "FooBar@gmail.com"
		}.also { mock ->
			creator.prepareNodes(entries).forEach { entity ->
				val item = creator.createNode(mock, entity)
				verifyAll {
					item.newInnerEntity("p1", "P1").newLiteral("x1").value = 111
					item.newInnerEntity("p4", "P2").newLiteral("x2").value = "AAA"
					item.newLiteral("creator").value = "googleDrive:FooBar@gmail.com"
				}
			}
		}
	}

	@Test
	fun `Create 1 hierarchical node with begin`() {
		val creator = NodeCreator(listOf(
				DefLine(DefFlag.USER_ID, "TABLE1", "UID", listOf(TypeDef("N1")), "string", "", listOf()),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL1", listOf(TypeDef("N1")), "datetime", "",
						listOf(LinkDef("begin", null))),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL3", listOf(TypeDef("N1")), "string", "",
						listOf(LinkDef("p4", "P2"),
								LinkDef("x2", null)))), null)

		val date = DateTime.parse("2019-12-11T12:34:56Z").toString()
		val entries = listOf(
				DbRecord("TABLEX", mapOf("COLX" to TypedLiteral.of("X"))),
				DbRecord("TABLE1", mapOf(
						"COL3" to TypedLiteral.of("AAA"),
						"COL1" to TypedLiteral(LiteralType.DATETIME, date))))

		mockk<EntityNode>(relaxed = true) {
			every { storage.type.name } returns "googleDrive"
			every { storage.userId } returns "FooBar@gmail.com"
		}.also { mock ->
			creator.prepareNodes(entries).forEach { entity ->
				val item = creator.createNode(mock, entity)
				verifyAll {
					item.setBegin(date)
					item.newInnerEntity("p4", "P2").newLiteral("x2").value = "AAA"
					item.newLiteral("creator").value = "googleDrive:FooBar@gmail.com"
				}
			}
		}
	}

	@Test
	fun `Create 1 hierarchical node (depth 2)`() {
		val creator = NodeCreator(listOf(
				DefLine(DefFlag.USER_ID, "TABLE1", "UID", listOf(TypeDef("Event"), TypeDef("N1")), "string", "", listOf()),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL1", listOf(TypeDef("Event"), TypeDef("N1")), "integer", "",
						listOf(LinkDef("p1", "P1"),
								LinkDef("x1", "integer"))),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL3", listOf(TypeDef("Event"), TypeDef("N1")), "string", "",
						listOf(LinkDef("p4", "P2"),
								LinkDef("x2", "PP2"),
								LinkDef("xx2", "duration"))),
				DefLine(DefFlag.MAPPING, "TABLE1", "COL4", listOf(TypeDef("Event"), TypeDef("N1")), "string", "",
						listOf(LinkDef("p4", "P2"),
								LinkDef("x3", "PP4"),
								LinkDef("xx4", "datetime")))), null)

		val duration1 = Duration.parse("PT200S").toString()
		val date1 = DateTime.parse("2019-11-11T12:34:56.000Z").toString()
		val entries = listOf(
				DbRecord("TABLEX", mapOf("COLX" to TypedLiteral.of("X"))),
				DbRecord("TABLE1", mapOf(
						"COL1" to TypedLiteral.of(111),
						"COL3" to TypedLiteral(LiteralType.DURATION, duration1),
						"COL4" to TypedLiteral(LiteralType.DATETIME, date1))))

		mockk<EntityNode>(relaxed = true) {
			every { storage.type.name } returns "googleDrive"
			every { storage.userId } returns "FooBar@gmail.com"
		}.also { mock ->
			creator.prepareNodes(entries).forEach { entity ->
				val item = creator.createNode(mock, entity)
				verifyAll {
					item.newLiteral("creator").value = "googleDrive:FooBar@gmail.com"
					item.newInnerEntity("p1", "P1").newLiteral("x1").value = 111
					item.newInnerEntity("p4", "P2").also { x ->
						x.newInnerEntity("x2", "PP2").newLiteral("xx2").value = duration1
						x.newInnerEntity("x3", "PP4").newLiteral("xx4").value = date1
					}
				}
			}
		}
	}
}

